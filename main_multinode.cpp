
#include "solctra_multinode.h"
#include <fstream>
#include <mpi.h>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <math.h>
#include <ctime>


const unsigned DEFAULT_STRING_BUFFER = 100;
const unsigned DEFAULT_STEPS = 500000;
const double DEFAULT_STEP_SIZE = 0.001;
const unsigned DEFAULT_PRECISION = 5;
const unsigned DEFAULT_PARTICLES= 1;
const unsigned DEFAULT_MODE= 1;
const std::string DEFAULT_OUTPUT = "results";
const std::string DEFAULT_RESOURCES = "resources";
const unsigned DEFAULT_MAGPROF = 0;
const unsigned DEFAULT_NUM_POINTS = 10000;
const unsigned DEFAULT_PHI_ANGLE = 0;
const unsigned DEFAULT_PRINT_TYPE = 0;
const unsigned DEFAULT_DIMENSION = 1;


unsigned getPrintPrecisionFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-precision")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_PRECISION;
}
unsigned getStepsFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-steps")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_STEPS;
}
double getStepSizeFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-stepSize")
        {
            return strtod(argv[i+1], nullptr);
        }
    }
    return DEFAULT_STEP_SIZE;
}
void LoadParticles(const int& argc, char** argv, Coil& particles, const int length)
{
    bool found = false;
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-particles")
        {
            loadFile(particles.x, particles.y, particles.z, length, argv[i+1]);
            found = true;
            break;
        }
    }
    if(!found)
    {
        printf("ERROR: particles path must be given!!\n");
        exit(1);
    }
}

std::string getResourcePath(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string param = argv[i];
        if("-resource" == param)
        {
            return std::string(argv[i+1]);
        }
    }
    return DEFAULT_RESOURCES;
}

unsigned getParticlesLengthFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-length")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    printf("ERROR: length of particles path must be given!!\n");
    exit(1);
}
unsigned getModeFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-mode")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_MODE;
}

std::string getJobId(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
        std::string tmp = argv[i];
        if(tmp == "-id")
        {
            return std::string(argv[i+1]);
        }
    }
    printf("ERROR: job id must be given!!\n");
    exit(1);
}

unsigned getMagneticProfileFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
     	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_MAGPROF;
}


unsigned getNumPointsFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
     	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
            return static_cast<unsigned>(atoi(argv[i+2]));
        }
    }
    return DEFAULT_NUM_POINTS;
}

unsigned getAngleFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
       	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
     	    return static_cast<unsigned>(atoi(argv[i+3]));
        }
    }
    return DEFAULT_PHI_ANGLE;
}

unsigned getDimension(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
     	std::string tmp = argv[i];
        if(tmp == "-magnetic_prof")
        {
            return static_cast<unsigned>(atoi(argv[i+4]));
        }
    }
    return DEFAULT_DIMENSION;
}

unsigned getPrintTypeFromArgs(const int& argc, char** argv)
{
    for(int i = 1 ; i < argc - 1 ; ++i)
    {
     	std::string tmp = argv[i];
        if(tmp == "-print_type")
        {
            return static_cast<unsigned>(atoi(argv[i+1]));
        }
    }
    return DEFAULT_PRINT_TYPE;
}



int main(int argc, char** argv)
{
    /*****MPI variable declarations and initializations**********/
    int provided;
    MPI_Init_thread(&argc, &argv,MPI_THREAD_FUNNELED,&provided);
    int myRank;
    int commSize;
    int nameLen;
    char processorName[MPI_MAX_PROCESSOR_NAME];
    MPI_Comm_size(MPI_COMM_WORLD, &commSize);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    MPI_Get_processor_name(processorName, &nameLen);
    /*****MPI variable declarations and initializations**********/

    /*******Declaring program and runtime parameters*************/	 
    std::string resourcePath; //Coil directory path
    unsigned steps; //Amount of simulation steps
    double stepSize; //Size of each simulation step
    
    /*Variables for magnetic profile diagnostic*/
    unsigned magprof; //Flag to control whether magnetic profile is computed
    unsigned num_points; //Number of sampling points for magnetic profile
    unsigned phi_angle; //Angle at which the magnetic profile will be computed
    /******************************************/
    
    unsigned precision; //TBD
    unsigned int length; //Amount of particles to simulate
    unsigned int mode; //Check divergence of simulation or not
    unsigned int print_type; //Printing flag: commas or tabs separators
    unsigned int dimension;
    std::string output; //Path of results directory
    std::string jobId; //JobID in the cluster
    std::ofstream handler;
    /*******Declaring program and runtime parameters*************/ 



    //Rank 0 reads input parameters from the command line
    //A log file is created to document the runtime parameters
    if(myRank == 0)
    {

	resourcePath = getResourcePath(argc, argv);
    	steps = getStepsFromArgs(argc, argv);
    	stepSize = getStepSizeFromArgs(argc, argv);
    	precision = getPrintPrecisionFromArgs(argc, argv);
    	length = getParticlesLengthFromArgs(argc, argv);
    	mode = getModeFromArgs(argc, argv);
    	print_type = getPrintTypeFromArgs(argc, argv);
    	magprof = getMagneticProfileFromArgs(argc, argv);
    	num_points = getNumPointsFromArgs(argc, argv);
    	phi_angle = getAngleFromArgs(argc, argv);
    	jobId = getJobId(argc, argv);
    	printf("Reading dimension parameter");
    	dimension = getDimension(argc,argv);
    	printf("Dimension parameter read: %d\n",dimension);
    	output = "results_" + jobId;
    	createDirectoryIfNotExists(output);
    	
	std::cout.precision(precision);
    	std::cout << "Communicator Size=[" << commSize << "]." << std::endl;
	std::cout << "Running with:" << std::endl;
    	std::cout << "Resource Path=[" << resourcePath << "]." << std::endl;
    	std::cout << "JobId=[" << jobId << "]." << std::endl;
    	std::cout << "Steps=[" << steps << "]." << std::endl;
    	std::cout << "Steps size=[" << stepSize << "]." << std::endl;
    	std::cout << "Particles=[" << length << "]." << std::endl;
    	std::cout << "Input Current=[" << I << "] A." << std::endl;
    	std::cout << "Mode=[" << mode << "]." << std::endl;
    	std::cout << "Output path=[" << output << "]." << std::endl;
    	std::string file_name = "stdout_"+jobId+".log";
    
    	handler.open(file_name.c_str());
    	if(!handler.is_open()){
        	std::cerr << "Unable to open stdout.log for appending. Nothing to do." << std::endl;
        	exit(0);
    	}

    	handler << "Running with:" << std::endl;
    	handler << "Steps=[" << steps << "]." << std::endl;
    	handler << "Steps size=[" << stepSize << "]." << std::endl;
    	handler << "Particles=[" << length << "]." << std::endl;
    	handler << "Mode=[" << mode << "]." << std::endl;
    	handler << "Output path=[" << output << "]." << std::endl;
	handler << "MPI size=[" << commSize << "]." << std::endl;
	handler << "Rank=[" << myRank << "] => Processor Name=[" << processorName << "]." << std::endl;

    }	

   
    std::cout<<"Rank=[" << myRank << "] => Processor Name=[" << processorName << "]." << std::endl;
    

    /*********** Rank 0 distributes runtime parameters amongst ranks********/
    MPI_Bcast(&steps, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    MPI_Bcast(&stepSize, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(&precision, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    MPI_Bcast(&length, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    MPI_Bcast(&mode, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    MPI_Bcast(&print_type, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);

	
    unsigned int outputSize = static_cast<unsigned int>(output.size());
    MPI_Bcast(&outputSize, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
    char* tmp = new char[outputSize + 1];
    if(0 == myRank)
    {
        std::strcpy(tmp, output.c_str());
    }
    MPI_Bcast(tmp, outputSize + 1, MPI_CHAR, 0, MPI_COMM_WORLD);
    if(0 != myRank)
    {
       output = std::string(tmp);
    }
    delete[] tmp;
    /*********** Rank 0 distributes runtime parameters amongst ranks********/
   

    /*********** Rank 0 reads in all particles ******/
    Coil particles;
    particles.x = static_cast<double*>(_mm_malloc(sizeof(double) * length, ALIGNMENT_SIZE));
    particles.y = static_cast<double*>(_mm_malloc(sizeof(double) * length, ALIGNMENT_SIZE));
    particles.z = static_cast<double*>(_mm_malloc(sizeof(double) * length, ALIGNMENT_SIZE));

    //Only rank 0 reads the information from the input file
    if(myRank==0)
    {
    	std::cout << "Loading Particles" << std::endl;
    	LoadParticles(argc, argv, particles, length);
    	std::cout << "Particles Loaded" << std::endl;
    }
    /*********** All ranks declare a memory space for their particle ******/
    

    /*********** Calculating distribution parameters for scatterv of particles********/
    int myShare = floor(length/commSize);
    int startPosition;        

    if(myRank < length%commSize)
    {
	myShare = myShare + 1;

    }

    int *groupmyShare = new int[commSize] ();
    int *displacements = new int[commSize] (); 

    MPI_Gather(&myShare, 1, MPI_INT, groupmyShare, 1, MPI_INT, 0, MPI_COMM_WORLD);

    if(myRank == 0)
    {
	std::cout << "Rank 0: Group Sizes are: " ;
        for(int i=0; i < commSize; i++)
        {
		std::cout << groupmyShare[i] << " ";

	} std::cout << std::endl;
	for(int i=1; i < commSize; i++)
        {
		displacements[i] = displacements[i-1]+groupmyShare[i-1];
		std::cout << "Rank " << i << " displacement: " << displacements[i] << std::endl;
        }

    }

    MPI_Bcast(displacements,commSize, MPI_INT, 0, MPI_COMM_WORLD);
    startPosition = displacements[myRank];
    std::cout << "Rank " << myRank << " startPosition: " << startPosition << std::endl;
    
    Coil localParticles;
    localParticles.x = static_cast<double*>(_mm_malloc(sizeof(double) * myShare, ALIGNMENT_SIZE));
    localParticles.y = static_cast<double*>(_mm_malloc(sizeof(double) * myShare, ALIGNMENT_SIZE));
    localParticles.z = static_cast<double*>(_mm_malloc(sizeof(double) * myShare, ALIGNMENT_SIZE));

    MPI_Scatterv(particles.x, groupmyShare, displacements, MPI_DOUBLE, localParticles.x, myShare, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatterv(particles.y, groupmyShare, displacements, MPI_DOUBLE, localParticles.y, myShare, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatterv(particles.z, groupmyShare, displacements, MPI_DOUBLE, localParticles.z, myShare, MPI_DOUBLE, 0, MPI_COMM_WORLD);
   /*********** Calculating distribution parameters for scatterv of particles********/


    /*********** All ranks declare a memory space for Coil data ******/
    const size_t sizeToAllocate = sizeof(double) * TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS;
    GlobalData data;
    data.coils.x = static_cast<double*>(_mm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    data.coils.y = static_cast<double*>(_mm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    data.coils.z = static_cast<double*>(_mm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    
    //Only Rank 0 reads the information from the coil files
    if(myRank == 0)
    {
    	std::cout << "Loading Coils" << std::endl;
    	load_coil_data(data.coils.x, data.coils.y, data.coils.z, resourcePath);
    	std::cout << "Coil data loaded" << std::endl;
    }
    //Rank 0 distributes coil data among ranks
    MPI_Bcast(data.coils.x, TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(data.coils.y, TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(data.coils.z, TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    /*********** All ranks declare a memory space for Coil data ******/	




    /*********** All ranks declare a memory space for e_roof computations ******/ 
    data.e_roof.x = static_cast<double*>(_mm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    data.e_roof.y = static_cast<double*>(_mm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    data.e_roof.z = static_cast<double*>(_mm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    data.leng_segment = static_cast<double*>(_mm_malloc(sizeToAllocate, ALIGNMENT_SIZE));
    
    if(myRank == 0){
    	std::cout << "Preparing eroof" << std::endl;
    	e_roof(data);
    	std::cout << "e_roof data loaded" << std::endl;	    
    }
    MPI_Bcast(data.e_roof.x, TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(data.e_roof.y, TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(data.e_roof.z, TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(data.leng_segment, TOTAL_OF_GRADES_PADDED * TOTAL_OF_COILS, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    /*********** All ranks declare a memory space for e_roof computations ******/


    //If the magneticProfile is requested, only rank 0 computes it.
    if(myRank == 0 && magprof != 0)
    {
	std::cout << "Computing magnetic profiles" << std::endl;    
	getMagneticProfile(data, num_points, phi_angle, output, dimension);
    }

    
    double startTime = 0;
    double endTime = 0;
    if(myRank==0)
    {
    	startTime= getCurrentTime();
    }
    std::cout << "Executing simulation" << std::endl;
    runParticles(data, output, localParticles, myShare, steps, stepSize, mode, print_type,startPosition);
    std::cout << "Simulation finished" << std::endl;
    
    MPI_Barrier(MPI_COMM_WORLD); 
    


    _mm_free(data.coils.x);
    _mm_free(data.coils.y);
    _mm_free(data.coils.z);
    _mm_free(data.e_roof.x);
    _mm_free(data.e_roof.y);
    _mm_free(data.e_roof.z);
    _mm_free(data.leng_segment);
    _mm_free(particles.x);
    _mm_free(particles.y);
    _mm_free(particles.z);
    _mm_free(localParticles.x);
    _mm_free(localParticles.y);
    _mm_free(localParticles.z);

    endTime= getCurrentTime();

    if(myRank == 0){
        std::cout << "Total execution time=[" << (endTime - startTime) << "]." << std::endl;
        handler << "Total execution time=[" << (endTime - startTime) << "]." << std::endl;
    	handler.close();
    	handler.open("stats.csv", std::ofstream::out | std::ofstream::app);
    	if(!handler.is_open())
    	{
        	std::cerr << "Unable to open stats.csv for appending. Nothing to do." << std::endl;
        	exit(0);
    	}
    	handler << jobId << "," << length << "," << steps << "," <<  stepSize << "," << output << "," << (endTime - startTime) << std::endl;
    	handler.close();
	time_t now = time(0);
	char* dt = ctime(&now);

	std::cout << "Timestamp: " << dt << std::endl;
    }
    

    MPI_Finalize();
    return 0;
}
